import React, { Component } from 'react';
import firebase from 'firebase';

export default class Skill extends Component {
    constructor() {
      super()
      this.state = {
        assetts   : "",
        languages : [],
        hobbies   : [],
        skills    : []
      }
    }

    componentWillMount() {
        let dataAsset = [];
        let dataLang  = [];
        let dataHobi  = [];
        let dataSkill = [];

        firebase.database().ref('assetts').on('value', snapshot => {
            const result = snapshot.val()

            if (result !== null) {
              result.forEach(value => {
                dataAsset.push(value.value)
              });
            }

            this.setState({
              assetts: dataAsset.join(', ')
            })
        })

        firebase.database().ref('languages').on('value', snapshot => {
            const result = snapshot.val()

            if (result !== null) {
              result.forEach(value => {
                dataLang.push(value)
              });
            }

            this.setState({
              languages: dataLang
            })
        })

        firebase.database().ref('hobbies').on('value', snapshot => {
            const result = snapshot.val()

            if (result !== null) {
              result.forEach(value => {
                dataHobi.push(value)
              });
            }

            this.setState({
              hobbies: dataHobi
            })
        })

        firebase.database().ref('skills').on('value', snapshot => {
            const result = snapshot.val()

            if (result !== null) {
              result.forEach(value => {
                dataSkill.push(value)
              });
            }

            this.setState({
              skills: dataSkill
            })
        })
    }

    _listLanguage() {
        let listLang = this.state.languages
        let languages     = []

        listLang.map((data, index) => {
            return languages.push(
            <li className="emph-1" key={ index }>{ data.lang } <span className="emph-4">( { data.strength } )</span></li>
            )
        })

        return languages
    }

    _listHobby() {
        let listHobby = this.state.hobbies
        let hobbies     = []

        listHobby.map((data, index) => {
            return hobbies.push(
                <li key={ index }>
                <span dangerouslySetInnerHTML={{__html: data.icon}}></span>
                <h6>{ data.hobby }</h6>
                </li>
            )
        })
      return hobbies
    }

     _listSkill() {
        let listSkill = this.state.skills
        let skills    = []
        let total     = 10

        listSkill.map((data, index) => {
            let can    = []
            let cannot = []

            let sisa = total - parseInt(data.strength)

            // can
            for (var i = 1; i <= parseInt(data.strength); i++) {
                can.push(<span key={ i }></span>)
            }

            // cannot
            for (var a = 1; a <= parseInt(sisa); a++) {
                cannot.push(<span key={ a } className="transparent"></span>)
            }

            return skills.push(
                <li key={ index }>
                    <h5>{ data.skill }</h5>
                    <div className="rating">
                        { can }
                        { cannot }
                    </div>
                </li>
            )
        })

        return skills
    }



    render() {
        return (
            <div className="block-content mb-100  pb-30">
                <div className="row">
                    <div className="col-md-12  ">
                        <div className="sub-title mb-40">
                            <h2 className="uppercase">skills</h2>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12  ">
                        <div className="listing-large mt-40">
                            {/*<a className="uppercase emph-1 btn-1" href="#">Download my cv</a>*/}
                            {/*<a className="uppercase emph-1 btn-2" href="#">Print My resume</a>*/}
                            <div className="listing-large-inner">
                                <div className="listing-event">
                                    <ul className="data left clearfix">
                                        { this._listSkill() }
                                    </ul>

                                    <ul className="data right clearfix">
                                        <li>
                                            <h5>Assests</h5>
                                            <p className="emph-3">{ this.state.assetts }</p>
                                        </li>

                                        <li>
                                            <h5>Languages</h5>
                                            <ul>
                                                { this._listLanguage() }
                                            </ul>
                                        </li>

                                        <li>
                                            <h5>Hobbies &amp; Interests</h5>
                                            <ul className="hb-list">
                                                { this._listHobby() }
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        );
    }
}
