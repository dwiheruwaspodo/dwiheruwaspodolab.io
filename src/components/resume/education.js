import React, { Component } from 'react';
import firebase from 'firebase';

export default class Education extends Component {
    constructor() {
      super()
      this.state = {
        educations : []
      }
    }

    componentWillMount() {
      let data = [];

      firebase.database().ref('educations').on('value', snapshot => {
        const result = snapshot.val()

        if (result !== null) {
          result.forEach(value => {
            data.push(value)
          });
        }

        this.setState({
          educations: data
        })
      })
    }

    _listSchools() {
      
      let listSchools = this.state.educations
      let educations  = []

      listSchools.map((data, index) => {
        let cur = data.date_end
        let cl  = "name"

        if (data.date_end === "current") {
          cur = <em>Current</em>
        }

        if (index % 2 === 0) {
          cl = "name switched"
        }

        return educations.push(
          <div className="timeline-inner" key={ index }>
              <div className={ cl }>
                  <span className="date">{ data.date_start } - { cur }</span>
                  <h4>{ data.major } – { data.school } </h4>
              </div>
              <div className="detail">
                  <p>{ data.description } </p>
              </div>
          </div>
        )
      })

      return educations
    }

    render() {
        return (
            <div className="block-content mb-100  pb-10">
                <div className="row">
                    <div className="col-md-12  ">
                        <div className="sub-title mb-40">
                            <h2 className="uppercase">Education</h2>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12  ">
                        <div className="timeline">
                            { this._listSchools() }
                        </div>
                    </div>
                </div>
            </div>  
        );
    }
}
