import React, { Component } from 'react';

export default class Services extends Component {
  render() {
    return (
        <div className="block-content mb-50  pb-30">
            <div className="row">
                <div className="col-md-12">
                    <div className="sub-title mb-40">
                        <h2 className="uppercase">Services</h2>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-md-12">
                    <div className="services mt-30">
                        <div className="block-service clearfix">
                            <div className="col-sm-6 text-center">
                                <div className="ico mb-15">
                                    <i className="icon-thumbs-up-5"></i>
                                </div>
                                <div className="det">
                                    <h5 className="mb-10">Great Support</h5>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>
                                </div>
                            </div>


                            <div className="col-sm-6 text-center">
                                <div className="ico mb-15">
                                    <i className="icon-pencil-7"></i>
                                </div>

                                <div className="det">
                                    <h5 className="mb-10">Print &amp; Branding</h5>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>
                                </div>

                            </div>


                            <div className="col-sm-6 text-center">
                                <div className="ico mb-15">
                                    <i className="icon-megaphone-3"></i>
                                </div>

                                <div className="det">
                                    <h5 className="mb-10">Marketing</h5>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>
                                </div>

                            </div>

                            <div className="col-sm-6 text-center">
                                <div className="ico mb-15">
                                    <i className="icon-desktop-3"></i>
                                </div>

                                <div className="det">
                                    <h5 className="mb-10">Web Design</h5>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>
                                </div>

                            </div>

                            <div className="col-sm-6 text-center">
                                <div className="ico mb-15">
                                    <i className="icon-params"></i>
                                </div>

                                <div className="det">
                                    <h5 className="mb-10">Development</h5>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>
                                </div>

                            </div>


                            <div className="col-sm-6 text-center">
                                <div className="ico mb-15">
                                    <i className="icon-lightbulb-3"></i>
                                </div>

                                <div className="det">
                                    <h5 className="mb-10">Branding</h5>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}
