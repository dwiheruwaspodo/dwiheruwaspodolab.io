import React, { Component } from 'react';

export default class NumberPages extends Component {
  render() {
    return (
      <div className="block-content">
          <div className="row">
              <div className="col-md-12">
                  <span className="page-number emph-1">- { this.props.currentPage }/{ this.props.totalPage } -</span>
              </div>
          </div>
      </div>
    );
  }
}
