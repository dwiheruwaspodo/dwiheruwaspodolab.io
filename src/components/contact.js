import React, { Component } from 'react';
import NumberPages from "./page-number";

export default class Contact extends Component {
  constructor() {
    super()
    
    this.state = {
      currentPage: 4
    }
  }

  render() {
    return (
      <section id="contact" className="bg-white t-center">
          <div className="contact">
              <div className="content">
                  <div className="block-content mb-100">
                      <div className="row">
                          <div className="col-md-12">
                              <div className="main-title">
                                  <h1 className="mb-20">Contact</h1>
                                  <h5 className="uppercase">Say Hello</h5>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div className="block-content mb-100 ">
                      <div className="row">
                          <div className="col-md-12  ">
                              <div className="sub-title mb-40">
                                  <h2 className="uppercase">Personal Info</h2>
                              </div>
                          </div>
                      </div>

                      <div className="row">
                          <div className="block-info ">
                              <div className="col-md-4">
                                  <div className="info">
                                      <div className="ico mb-20">
                                          <i className="icon-location-7"></i>
                                      </div>
                                      <h5 className="mb-20">Drop on in</h5>
                                      <p>
                                          { this.props.domicile }
                                      </p>
                                  </div>
                              </div>

                              <div className="col-md-4 ">
                                  <div className="info">
                                      <div className="ico mb-20">
                                          <i className="icon-mobile-6"></i>
                                      </div>
                                      <h5 className="mb-20">Give me a call</h5>
                                      <p>
                                          <br /> Mobile :<br/> { this.props.phone }
                                      </p>
                                  </div>
                              </div>

                              <div className="col-md-4">
                                  <div className="info">
                                      <div className="ico mb-20">
                                          <i className="icon-paper-plane-3"></i>
                                      </div>
                                      <h5 className=" mb-20">Let's connect</h5>
                                      <p>
                                          <br /> Email:<br/> { this.props.email }
                                      </p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <NumberPages currentPage={ this.state.currentPage } totalPage={ this.props.totalPage } />
              </div>
          </div>
      </section>
    );
  }
}
