import React, { Component } from 'react';
import NumberPages from "./page-number";
import firebase from 'firebase';

export default class Portfolio extends Component {
  constructor() {
    super()
    
    this.state = {
      currentPage: 3,
      projects : [],
      category : [],
      photoProj: null
    }
  }

  componentWillMount() {
    let totalWork = 0
    let projects  = []
    let category  = []

    const connect = firebase.database().ref('works')

    connect.on('value', snapshot => {
      totalWork = snapshot.val().length

      for (var i=0; i < totalWork; i++) {
        connect.child(i).child('projects').on('value', snapshot => {
          snapshot.val().map((data) => {
            category.push(data.role)
            projects.push(data)
          })
        })
      }

      this.setState({
        projects: projects,
        category: [...new Set(category)]
      })
    })

    firebase.storage().ref('portfolio/2.jpg').getDownloadURL().then((url) => {
      this.setState({
        photoProj: url
      })
    })
  }

  _listCategory() {
    let category = [];
    this.state.category.map((data, index) => {
      let href = "#"+data
        return category.push(
          <li key={index}><a href={ href } data-filter={data}>{data}</a></li>
        )
      }
    )

    return category;
  }

  _listProject() {
    let projects = [];
    this.state.projects.map((data, index) => {
        let item = "item " + data.role

        return projects.push(
          <li className="col-sm-6" key={index}>
              <div className={ item }>
                  <a href={ null } className="popup-vimeo">
                      <div className="desc">
                          <h5 className="proj-desc">{ data.name }
                              <span>{ data.desc }</span></h5>
                      </div>
                      <img src={ this.state.photoProj } alt="" />
                  </a>
              </div>
          </li>
        )
      }
    )

    return projects;
  }

  render() {
    return (
      <section id="portfolio" className="bg-white t-center">

          <div className="portfolio">
              <div className="content">
                  <div className="block-content mb-100">
                      <div className="row">
                          <div className="col-md-12">
                              <div className="main-title ">
                                  <h1 className="mb-20">Portfolio</h1>
                                  <h5 className="uppercase">My Works</h5>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div className="block-content mb-100">
                      <div className="works">
                          <div className="row">
                              <div className="col-md-12">
                                  <div className="block-filter uppercase mb-40">
                                      <ul className="filter" id="category">
                                          <li><a className="active" href="#all" data-filter="all">all</a></li>
                                          { this._listCategory() }
                                      </ul>
                                  </div>
                              </div>
                          </div>

                          <div className="row">
                              <div className="col-md-12">
                                  <ul className="work">
                                    { this._listProject() }
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>

                  <NumberPages currentPage={ this.state.currentPage } totalPage={ this.props.totalPage } />

              </div>

          </div>


      </section>
    );
  }
}
