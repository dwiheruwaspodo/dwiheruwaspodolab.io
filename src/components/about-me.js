import React, { Component } from 'react';
import NumberPages from "./page-number";
import firebase from 'firebase';

export default class AboutMe extends Component {
  constructor() {
    super()
    
    this.state = {
      currentPage: 1,
      name        : "",
      role        : "",
      aboutme     : "",
      birth_day   : "",
      birth_place : "",
      domicile    : "",
      marriage    : "",
      freelance   : "",
      country     : "",
      signature   : null
    }
  }

  componentWillMount() {
    firebase.database().ref('bio').on('value', snapshot => {
      const result = snapshot.val()

      if (result !== null) {
        this.setState({
          name        : result.name,
          role        : result.role,
          aboutme     : result.aboutme,
          birth_day   : result.birth_date,
          birth_place : result.birth_place,
          country     : result.country,
          domicile    : result.domicile,
          marriage    : result.marriage,
          freelance   : result.freelance
        })
      }
    })

    firebase.storage().ref('signature.png').getDownloadURL().then((url) => {
      this.setState({
        signature: url
      })
    })
  }

  render() {
    let website02 = []

    if (this.props.website02 !== "") {
      website02.push(<span> <br /> {this.props.website02} </span>)
    }

    return (
      <section id="about" className="bg-white t-center">
          <div className="about">
              <div className="content">
                  <div className="block-content mb-100">
                      <div className="row">
                          <div className="col-md-12  ">
                              <div className="main-title profile">
                                  <h1 className="mb-20">{ this.state.name }</h1>
                                  <h3>{ this.state.role }</h3>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div className="block-content mb-100">
                      <div className="row">
                          <div className="col-md-12  ">
                              <div className="sub-title mb-40">
                                  <h2 className="uppercase">About Me</h2>
                              </div>
                          </div>
                      </div>
                      <p className="lead-intro">“ { this.state.aboutme } “</p>
                      <img className="signature mt-20" src={ this.state.signature } alt="" />
                  </div>

                  <div className="block-content mb-100 pb-30">
                      <div className="row">
                          <div className="col-md-12  ">
                              <div className="sub-title mb-40">
                                  <h2 className="uppercase">Personal Info</h2>
                              </div>
                          </div>
                      </div>
                      <div className="row">
                          <div className="col-md-8 col-md-offset-3  col-sm-8 col-sm-offset-3 ">
                              <div className="listing mt-40">
                                  <div className="listing-inner">
                                      <div className="listing-event">
                                          <ul className="data left">
                                              <li className="emph-1">Name</li>
                                              <li className="emph-1">Birthday</li>
                                              <li className="emph-1">Place of Birth</li>
                                              <li className="emph-1">Nationality</li>
                                              <li className="emph-1">Marital Status</li>
                                              <li className="emph-1">Freelance</li>
                                          </ul>

                                          <ul className="data right">
                                              <li>{ this.state.name }</li>
                                              <li>{ this.state.birth_day } <span className="emph-1">(year/month/day)</span></li>
                                              <li>{ this.state.birth_place }</li>
                                              <li>{ this.state.country }</li>
                                              <li>{ this.state.marriage }</li>
                                              <li>{ this.state.freelance }</li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div className="block-content mb-100  pb-30">
                      <div className="row">
                          <div className="col-md-12  ">
                              <div className="sub-title mb-40">
                                  <h2 className="uppercase">Contact Info</h2>
                              </div>
                          </div>
                      </div>
                      <div className="row">
                          <div className="col-md-8 col-md-offset-3  col-sm-8 col-sm-offset-3 ">
                              <div className="listing mt-40">
                                  {/*<a className="uppercase emph-1 btn-1">Download my cv</a>*/}
                                  {/*<a className="uppercase emph-1 btn-2">Print My resume</a>*/}
                                  <div className="listing-inner">
                                      <div className="listing-event">
                                          <ul className="data left">
                                              <li><span className="emph-1">Adress</span> :<br /> { this.state.domicile } </li>
                                          </ul>
                                          <ul className="data right">
                                              <li><span className="emph-1">Phone</span> :<br /> { this.props.phone } </li>
                                              <li><span className="emph-1">E-Mail</span> :<br /> { this.props.email }</li>
                                              <li><span className="emph-1">Website</span> :<br /> { this.props.website01 }
                                              { website02 } 
                                              </li>
                                              <li><span className="emph-1">Repository</span> :<br /> { this.props.repo } </li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <NumberPages currentPage={ this.state.currentPage } totalPage={ this.props.totalPage } />

              </div>
          </div>
      </section>
    );
  }
}
