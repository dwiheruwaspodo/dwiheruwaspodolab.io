import React, { Component } from 'react';
import NumberPages from "./page-number";

import Experience from './resume/experience';
import Education from './resume/education';
import Skill from './resume/skill';

export default class Resume extends Component {
  constructor() {
    super()
    
    this.state = {
      currentPage: 2
    }
  }

  render() {
    return (
      <section id="resume" className="bg-white t-center">
          <div className="resume">
              <div className="content">
                  <div className="block-content mb-100">
                      <div className="row">
                          <div className="col-md-12  ">
                              <div className="main-title">
                                  <h1 className="mb-20">Resume</h1>
                                  <h5 className="uppercase">Curiculum Vita</h5>
                              </div>
                          </div>
                      </div>
                  </div>
                  <Experience />
                  <Education />
                  <Skill />
                  <NumberPages currentPage={ this.state.currentPage } totalPage={ this.props.totalPage } />
              </div>
          </div>
      </section>
    );
  }
}
