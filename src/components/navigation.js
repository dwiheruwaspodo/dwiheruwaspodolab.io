import React, { Component } from 'react';
import firebase from 'firebase';

export default class Navigation extends Component {
  constructor() {
    super()
    this.state = {
      nav: "",
      urlPhoto: null
    }
  }

  componentWillMount() {
    this.setState({
      nav: sessionStorage.getItem('nav')
    })

    firebase.storage().ref('foto1.jpg').getDownloadURL().then((url) => {
      this.setState({
        urlPhoto: url
      })
    })
  }

  changeNav(val) {
    sessionStorage.setItem('nav', val);
  }

  render() {
    return (
      <div className="col-md-2 left-content pd-r0">
          <header id="header">
              <div className="main-header">
                  <div className="img-profile">
                      <img src={ this.state.urlPhoto } alt="" />

                      <div className="name-profile t-center">
                          <h5 className="uppercase">Dwi Heru Budi Waspodo</h5>
                      </div>
                  </div>

                  <nav id="main-nav" className="main-nav clearfix t-center uppercase tabbed">
                      <ul className="clearfix">
                          <li onClick={ this.changeNav.bind(this, "about") }><a className={ (this.state.nav === "about") ? "active" : "" } href="about"><i className="icon-user"></i>About me<span>who am i</span></a></li>
                          <li onClick={ this.changeNav.bind(this, "resume") }><a className={ (this.state.nav === "resume") ? "active" : "" } href="resume"><i className="icon-briefcase"></i>Resume<span>curiculum vita</span></a></li>
                          <li onClick={ this.changeNav.bind(this, "portfolio") }><a className={ (this.state.nav === "portfolio") ? "active" : "" } href="portfolio"><i className="icon-camera"></i>Portfolio<span>my works</span></a></li>
                          <li onClick={ this.changeNav.bind(this, "contact") }><a className={ (this.state.nav === "contact") ? "active" : "" } href="contact"><i className="icon-phone"></i>Contact<span>say hello</span></a></li>
                      </ul>
                  </nav>
              </div>
          </header>
      </div>
    );
  }
}