import React from "react";
import { Link } from 'react-router';
import firebase from 'firebase';
import { configs } from "../configs";

export default class PageNotFound extends React.Component {
  constructor() {
    super()
    firebase.initializeApp(configs.firebase);
    this.state = {
      urlPhoto: null
    }
  }

  componentWillMount() {
    firebase.storage().ref('hantu.png').getDownloadURL().then((url) => {
      this.setState({
        urlPhoto: url
      })
    })
  }

  render() {
    return (
      <div id="wrapper" className="mt-100 mb-150">
        <div className="col-md-8 text-center">
          <div style={{ marginTop: 100}}>
            <h1>
              404
            </h1>
            <h2>Halaman Tidak Ditemukan</h2>
            <small>Mainnya jangan kejauhan, dab!</small>
            <br/>
            <br/>
            <br/>
            <Link to="/" className="btn btn-danger">Kembali</Link>
          </div>
        </div>
        <div className="col-md-4 text-center">
          <img 
            src={ this.state.urlPhoto }
            alt="" 
            style={{ width:"270px", marginTop:"70px" }}
          />
        </div>
      </div>
    );
  }
}