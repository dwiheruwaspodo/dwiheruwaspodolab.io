import React, { Component } from 'react';

export default class Footer extends Component {
  render() {
    return (
      <footer className="footer ">
          <div className="col-md-1 pd-l0">
              <ul className="social">
                  <li><a href={ this.props.facebook } target="_blank" rel="noopener noreferrer"><i className="icon-facebook"></i></a></li>
                  <li><a href={ this.props.instagram } target="_blank" rel="noopener noreferrer"><i className="icon-instagram"></i></a></li>
                  <li><a href={ this.props.twitter } target="_blank" rel="noopener noreferrer"><i className="icon-twitter"></i></a></li>
                  <li><a href={ this.props.linkedin } target="_blank" rel="noopener noreferrer"><i className="icon-linkedin"></i></a></li>
                  <li><a href={ this.props.repo } target="_blank" rel="noopener noreferrer"><i className="icon-github"></i></a></li>
              </ul>
              <div className="copyright">
                  <p>© 2016 Mutationthemes</p>
              </div>
          </div>
      </footer>
    );
  }
}
