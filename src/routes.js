import React from 'react';
import { Route, IndexRoute } from "react-router";

import PageNotFound from "./components/page-not-found";
import App from "./App";

import About from "./components/about-me";
import Contact from "./components/contact";
import Portfolio from "./components/portfolio";
import Resume from "./components/resume";

export const routes = (
	<div>
		<Route path="/" component={App}>
			<IndexRoute component={ About } />
			<Route component={ Contact } path="contact"/>
			<Route component={ Portfolio } path="portfolio"/>
			<Route component={ Resume } path="resume"/>
			<Route component={ About } path="about"/>
		</Route>
		<Route path="*" component={PageNotFound} />
	</div>
);