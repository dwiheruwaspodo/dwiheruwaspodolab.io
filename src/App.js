import React, { Component } from 'react';
import Footer from './components/footer';
import Navigation from './components/navigation';
import firebase from 'firebase';
import { configs } from "./configs";

class App extends Component {
  constructor() {
    super()
    firebase.initializeApp(configs.firebase);

    this.state = {
      totalPage : 4,
      facebook  : "",
      instagram : "",
      linkedin  : "",
      twitter   : "",
      phone     : "",
      website01 : "",
      website02 : "",
      repo      : "",
      email     : "",
      domicile  : ""
    }
  }

  componentDidMount() {
    firebase.database().ref('contacts').on('value', snapshot => {
      const result = snapshot.val()

      if (result !== null) {
        this.setState({
          facebook  : result.facebook,
          instagram : result.instagram,
          twitter   : result.twitter,
          linkedin  : result.linkedin,
          phone     : result.phone,
          website01 : result.web_1,
          website02 : result.web_2,
          repo      : result.repo,
          email     : result.email
        })
      }
    })

    firebase.database().ref('bio').on('value', snapshot => {
      const result = snapshot.val()

      if (result !== null) {
        this.setState({
          domicile    : result.domicile,         
        })
      }
    })
  }

  render() {

    const children = React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        totalPage : this.state.totalPage,
        email     : this.state.email,
        phone     : this.state.phone,
        repo      : this.state.repo,
        website01 : this.state.website01,
        website02 : this.state.website02,
        domicile  : this.state.domicile,
      });
    });

    return (
      <div id="wrapper" className="mt-50 mb-50">
        <div className="container">        
          <div className="row ">
            <Navigation />

            <div className="col-md-9 right-content pd-r0 pd-l0">
              { children }
            </div>

            <Footer 
              facebook={ this.state.facebook }
              instagram={ this.state.instagram }
              twitter={ this.state.twitter }
              linkedin={ this.state.linkedin }
              repo={ this.state.repo }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
